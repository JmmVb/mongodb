package com.example.mongo.bean;

public class Client {

	private String name;
	private String lastName;
	private String numberOfDocument;
	private String phone;
	private String gender;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNumberOfDocument() {
		return numberOfDocument;
	}

	public void setNumberOfDocument(String numberOfDocument) {
		this.numberOfDocument = numberOfDocument;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
