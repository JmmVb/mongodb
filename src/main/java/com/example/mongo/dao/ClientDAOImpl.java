package com.example.mongo.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.example.mongo.bean.Client;
import com.example.mongo.configuration.MongoDBConfiguration;

public class ClientDAOImpl implements ClientDAO {

	ApplicationContext context;
	MongoOperations mongoOperations;

	public ClientDAOImpl() {
		context = new AnnotationConfigApplicationContext(
				MongoDBConfiguration.class);
		mongoOperations = (MongoOperations) context.getBean("mongoTemplate");
	}

	public String insertClient(Client client) {

		String responseMessage = "0";

		try {
			mongoOperations.save(client);
			responseMessage = "1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseMessage;
	}

	@Override
	public String updateClient(com.example.mongo.model.Client clientMongo) {
		String status = "0";
		com.example.mongo.model.Client client;
		
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("numberOfDocument").is(clientMongo.getNumberOfDocument()));
			client = mongoOperations.findOne(query, com.example.mongo.model.Client.class);
			client.setName(clientMongo.getName());
			client.setLastName(clientMongo.getLastName());
			mongoOperations.save(client);
			status = "1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}

}
