package com.example.mongo.controller;

import com.example.mongo.bean.Client;
import com.example.mongo.service.ClientService;
import com.example.mongo.service.ClientServiceImpl;

public class ClientController {

	private ClientService clientService;
	
	public String save(Client client) {
		clientService = new ClientServiceImpl();
		return clientService.insertClient(client);
	}

	public String update(Client client) {
		clientService = new ClientServiceImpl();
		return clientService.updateClient(client);
	}


	
}
