package com.example.mongo.service;

import com.example.mongo.bean.Client;

public interface ClientService {

	String insertClient(Client client);

	String updateClient(Client client);
}
