package com.example.mongo.service;

import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.mongo.bean.Client;
import com.example.mongo.configuration.ApplicationConfiguration;
import com.example.mongo.dao.ClientDAO;
import com.example.mongo.dao.ClientDAOImpl;
import com.example.mongo.transfer.ClientTransfer;

public class ClientServiceImpl implements ClientService {
	
	ClientDAO clientDAO = new ClientDAOImpl();
	ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

	public String insertClient(Client client) {

		com.example.mongo.model.Client clientMongo = new com.example.mongo.model.Client();
		clientMongo.setId(ObjectId.get());
		clientMongo.setGender(client.getGender());
		clientMongo.setLastName(client.getLastName());
		clientMongo.setName(client.getName());
		clientMongo.setNumberOfDocument(client.getNumberOfDocument());
		clientMongo.setPhone(client.getPhone());
		
		return clientDAO.insertClient(client);
	}

	@Override
	public String updateClient(Client client) {
		ClientTransfer clientTransfer = new ClientTransfer();
		com.example.mongo.model.Client clientRQ = clientTransfer.buildClientUpdate(client);
		return clientDAO.updateClient(clientRQ);
	}

}
